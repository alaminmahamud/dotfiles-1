```
      ██            ██     ████ ██  ██                
     ░██           ░██    ░██░ ░░  ░██                
     ░██  ██████  ██████ ██████ ██ ░██  █████   ██████
  ██████ ██░░░░██░░░██░ ░░░██░ ░██ ░██ ██░░░██ ██░░░░
 ██░░░██░██   ░██  ░██    ░██  ░██ ░██░███████░░█████
░██  ░██░██   ░██  ░██    ░██  ░██ ░██░██░░░░  ░░░░░██
░░██████░░██████   ░░██   ░██  ░██ ███░░██████ ██████
 ░░░░░░  ░░░░░░     ░░    ░░   ░░ ░░░  ░░░░░░ ░░░░░░  


░░░░░░░░░░
░ author ░ Marcel Robitaille
░ repo   ░ https://github.com/Iambecomeroot/dotfiles
░░░░░░░░░░
```

# My dotfiles, managed with GNU stow

```
bin       ➔ executable scripts
bspwm     ➔ window manager config
cava      ➔ Console-based Audio Visualizer for ALSA (MPD and Pulseaudio)
compton   ➔ display compositor
dunst     ➔ notification config and related icons
eslint    ➔ code linter
git       ➔ source control config and aliases
gtk       ➔ custom css for modifying gtk apps
lemonbar  ➔ lemon scented status bars
mpd       ➔ music server config
ncmpcpp   ➔ music player config
neofetch  ➔ system info script config
ranger    ➔ file manager config
refind    ➔ backup of my rEFInd config
rofi-pass ➔ rofi frontend for pass by carnager
stylish   ➔ custom css for websites
sxhkd     ➔ keybindings
termite   ➔ terminal emulator
tmux      ➔ terminal multiplexer
vim       ➔ terminal text editor
X         ➔ display server
zathura   ➔ document viewer config
zsh       ➔ shell
```

## Keybindings
I spend most of my time in a terminal emulator. For that reason, I have tried to keep the keybindings similar for different terminal applications just with different modifiers. I am often in vim under tmux. I move around in vim more so vim's keybindings don't have a modifier. The alt modifier will trigger a similar command in tmux and the super modifier will trigger it in bspwm. For example: in vim <kbd>\`</kbd> switches to the most recent buffer, in tmux, <kbd>alt-\`</kbd> switches to the most recent window, and in bspwm, <kbd>super-\`</kbd> switches to the most recent workspace. For a complete list of keybindings, see [keybindings.md](keybindings.md).

## Screenshots

### Clean
![scrot 0](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_21-31-37.png)

### atom & termite + neofetch
![scrot 1](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_19-12-52.png)

### 2x conky & tmux + 2x ncmpcpp
![scrot 2](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_19-21-01.png)

### tmux + a bunch of colour scripts
![scrot 3](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_19-32-17.png)

### uname + htop
![scrot 4](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_21-24-01.png)

### dunst + scripts to change workspace, volume, and brightness
![scrot 5](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/2016-12-31-194205_1920x1080_scrot.png)

### i3lock
![scrot 6](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/2016-12-31-190809_1920x1080_scrot.png)

### tmux music window (neofetch + 2x ncmpcpp + alsamixer)
![scrot 7](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_19-12-52.png)

### rofi
![scrot 8](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_19-22-08.png)

### rofi-pass as a frontend for pass
![scrot 9](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_19-22-51.png)

### My two favourite file managers (thunar on the left and ranger on the right)
![scrot 10](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_19-33-35.png)

### Favourite search engine
![scrot 11](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_19-35-43.png)

### Start page (left) and colour scheme (terminal, atom, and start page) (right)
![scrot 12](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_19-35-15.png)

### whisker menu
![scrot 13](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_19-23-48.png)

### Music conky. Read more [here](https://github.com/iambecomeroot/apollo).
![scrot 14](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_21-08-35.png)

### wallpapers
![scrot 15](https://raw.githubusercontent.com/Iambecomeroot/dotfiles/master/scrots/Screenshot_2016-12-31_19-38-33.png)
